import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Field pole = new Field();
        pole.emptypole();
        pole.startspawn(Field.startR, Field.rabbit);
        pole.startspawn(Field.startW, Field.wolf);
        int countIterations = vvod();
        for(int i=0; i<countIterations;i++)
        {
            pole.iteration();
            printField();
        }

    }


    public static int vvod()
    {
        System.out.print("Введите количество итераций -> ");
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }

        protected static void printField() {
        for (int i = 0; i < Field.size; i++) {
            for (int j = 0; j < Field.size; j++) {
                System.out.print(Field.pole[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}